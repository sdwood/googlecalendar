Vagrant
=======

A set of scripts used to setup Vagrant developer boxes.

Setup
=====

Pull down the Vagrant file to your own repo:
```
cd my-repo/

TMP_DIR=$( mktemp -d )
VAGRANT_REPO_TARBALL="https://a153ebbeb52d45bc411decce8715488f65d3964f:x-oauth-basic@code.mtroyal.ca/webteam/vagrant/tarball/master/"
curl --insecure --location --silent $VAGRANT_REPO_TARBALL  | tar zx --strip-components=1 -C $TMP_DIR
mv $TMP_DIR/Vagrantfile .

```

Edit the Vagrantfile, editing the IP address and commenting in any packages that you need.
