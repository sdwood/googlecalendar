
MAILTRAP_USER=$1
MAILTRAP_PASSWORD=$2

# Install postfix
yum -y install postfix
/sbin/chkconfig postfix on

# Install the main postfix config
tee -a /etc/postfix/main.cf << EOF
relayhost = [mailtrap.io]:2525
smtp_sasl_auth_enable = yes
smtp_sasl_mechanism_filter = plain
smtp_sasl_security_options = noanonymous
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
EOF

tee /etc/postfix/sasl_passwd << EOF
mailtrap.io $MAILTRAP_USER:$MAILTRAP_PASSWORD
EOF

/usr/sbin/postmap /etc/postfix/sasl_passwd
/etc/init.d/postfix restart
