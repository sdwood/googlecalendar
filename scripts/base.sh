set -e -u

HOST_NAME="$1"
SWAP_SIZE="$2"
TIMEZONE="$3"

copy_ssh_keys() {
    # Ensure the ssh keys are properly chmod'ed
    chmod 0700 /home/vagrant/.ssh/
    chmod 0600 /home/vagrant/.ssh/id_rsa

    # Copy the ssh keys to /root/ as well
    cp -r /home/vagrant/.ssh/ /root/
    chown root:root -R /root/.ssh/
    chmod 0700 /root/.ssh/
    chmod 0600 /root/.ssh/id_rsa
}

configure_network() {
    # Turn off iptables so it doesn't block incoming HTTP/S requests
    /etc/init.d/iptables stop
    /sbin/chkconfig iptables off

    # Add some aliases
    tee -a /etc/bashrc << EOF

# Development environment customizations
alias ll='ls -al --color=auto'
export PATH=\$PATH:/usr/sbin/:/sbin/:/usr/local/bin
EOF
    set +u
    source /etc/bashrc
    set -u

    # Change the hostname
    tee /etc/sysconfig/network << EOF
NETWORKING=yes
NETWORKING_IPV6=no
HOSTNAME=$HOST_NAME
EOF

    # Restart the network connection as it often hangs at this point waiting to do DNS lookup
    /etc/init.d/network restart
}

set_timezone_and_locale() {
    echo "Setting Timezone & Locale to $TIMEZONE & C.UTF-8"
    ln -sf /usr/share/zoneinfo/$TIMEZONE /etc/localtime
    export LANG=C.UTF-8

    echo "export LANG=C.UTF-8" >> /etc/bashrc
}

init_system_swap() {
    ## Setting up Swap

    # Disable case sensitivity
    shopt -s nocasematch

    if [[ ! -z $SWAP_SIZE && ! $SWAP_SIZE =~ false && $SWAP_SIZE =~ ^[0-9]*$ ]]; then

        echo ">>> Setting up Swap ($SWAP_SIZE MB)"

        # Create the Swap file
        SWAP_BLOCKS=$( expr $SWAP_SIZE \* 1024 )
        dd if=/dev/zero of=/swapfile bs=1024 count=$SWAP_BLOCKS

        # Set the correct Swap permissions
        chmod 600 /swapfile

        # Setup Swap space
        mkswap /swapfile

        # Enable Swap space
        swapon /swapfile

        # Make the Swap file permanent
        echo "/swapfile   none    swap    sw    0   0" | tee -a /etc/fstab

        # Add some swap settings:
        # vm.swappiness=10: Means that there wont be a Swap file until memory hits 90% useage
        # vm.vfs_cache_pressure=50: read http://rudd-o.com/linux-and-free-software/tales-from-responsivenessland-why-linux-feels-slow-and-how-to-fix-that
        printf "vm.swappiness=10\nvm.vfs_cache_pressure=50" | tee -a /etc/sysctl.conf && sysctl -p

    fi

    # Enable case sensitivity
    shopt -u nocasematch
}

install_base_utilities() {
    # Install utilities
    rm -f /var/cache/yum/timedhosts.txt
    yum -y update
    yum -y install multitail \
                   htop \
                   mc \
                   mailx
}

update_centos_repos() {
    wget http://dl.fedoraproject.org/pub/epel/5/x86_64/epel-release-5-4.noarch.rpm
    sudo rpm -Uvh epel-release-5*.rpm    
    
    wget http://rpms.famillecollet.com/enterprise/remi-release-5.rpm
    sudo rpm -Uvh remi-release-5*.rpm
}

###
# Main
###

copy_ssh_keys
configure_network
set_timezone_and_locale
init_system_swap
install_base_utilities
