set -e -u

ROOT_PASSWORD=$1
DB_NAME=$2
DB_USER=$3
DB_PASSWORD=$4

# Install MySQL
yum -y install mysql51-mysql-server

# Configure MySQL
/sbin/chkconfig mysql51-mysqld on
/etc/init.d/mysql51-mysqld start

# Create the logs, and make sure the vagrant user can read them
touch /opt/rh/mysql51/root/var/run/mysqld/mysqld.log
touch /opt/rh/mysql51/root/var/run/mysqld/mysqld-slow.log
chmod 666 /opt/rh/mysql51/root/var/run/mysqld/*.log
ln -sf /opt/rh/mysql51/root/var/run/mysqld/ /var/log/mysql

# Symlink in the path to the my.cnf that MySQL actually uses
rm -f /etc/my.cnf
ln -sf /opt/rh/mysql51/root/etc/my.cnf /etc/my.cnf

# Enable remote access to MySQL
sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/my.cnf

echo "
-- Secure the MySQL installation
DROP DATABASE IF EXISTS test;
DELETE FROM mysql.user WHERE User='root' AND Host!='localhost';
DELETE FROM mysql.user WHERE User='';

-- Create the root account with a password
GRANT ALL ON *.* TO 'root'@'localhost' IDENTIFIED BY '$ROOT_PASSWORD' WITH GRANT OPTION;
GRANT ALL ON *.* TO 'root'@'%' IDENTIFIED BY '$ROOT_PASSWORD' WITH GRANT OPTION;
" | mysql -uroot

if [ ! -z $DB_NAME ]; then
    echo "
-- Create the research database
CREATE DATABASE IF NOT EXISTS $DB_NAME;

-- Create the user and grant them privileges
GRANT USAGE ON *.* TO '$DB_USER'@'%' IDENTIFIED BY '$DB_PASSWORD';  -- GRANT USAGE is used here to create the user if it doesn't exist.
GRANT USAGE ON *.* TO '$DB_USER'@'localhost' IDENTIFIED BY '$DB_PASSWORD';

GRANT ALTER, INDEX, SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, CREATE TEMPORARY TABLES ON $DB_NAME.* to '$DB_USER'@'%';
GRANT ALTER, INDEX, SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, CREATE TEMPORARY TABLES ON $DB_NAME.* to '$DB_USER'@'localhost';

-- Reload the privileges
FLUSH PRIVILEGES;
" | mysql -uroot -p$ROOT_PASSWORD

    # Import the database dump, if it exists
    if [ -e /vagrant/dump.sql ]; then
        cat /vagrant/dump.sql | mysql -uroot -p$ROOT_PASSWORD $DB_NAME
    else
        echo "***** MySQL dump.sql not found, so not importing..."
    fi

fi
