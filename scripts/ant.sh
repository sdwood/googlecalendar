set -e -u

# Install Ant
mkdir /apache-ant
curl --insecure --silent https://archive.apache.org/dist/ant/binaries/apache-ant-1.8.4-bin.tar.gz | tar xz --strip-components=1 -C /apache-ant

# Setup the environment variables
tee -a /etc/bashrc << EOF

# Apache Ant
export ANT_HOME=/apache-ant
export PATH=\$PATH:\$ANT_HOME/bin
EOF

set +u
source /etc/bashrc
