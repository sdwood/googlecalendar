set -e -u

# Install Postgres
yum install -y postgresql84 postgresql84-server postgresql84-contrib
/sbin/service postgresql initdb

# Enable listening on *
sed -i -r "s/#listen_addresses = 'localhost'/listen_addresses = '\*'/" /var/lib/pgsql/data/postgresql.conf

# Update the access restrictions
tee /var/lib/pgsql/data/pg_hba.conf << EOF
# PostgreSQL Client Authentication Configuration File
# ===================================================
# TYPE  DATABASE    USER        CIDR-ADDRESS          METHOD

# "local" is for Unix domain socket connections only
local   all         all                               ident

# IPv4 local connections:
host    all         all         127.0.0.1/32          md5

# IPv6 local connections:
host    all         all         ::1/128               md5

# DSpace
host    dspace     dspace       127.0.0.1   255.255.255.255   md5

# Allow development client access
host    all        all          192.168.5.0/24 trust
EOF

/sbin/chkconfig postgresql on
/sbin/service postgresql start
