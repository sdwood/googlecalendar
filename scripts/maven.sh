set -e -u

# Install Apache Maven
wget --quiet https://archive.apache.org/dist/maven/binaries/apache-maven-2.2.1-bin.zip -O maven.zip
unzip maven.zip
mv apache-maven-* /apache-maven
rm -f maven.zip

# Setup the environment variables
tee -a /etc/bashrc << EOF

# Apache Maven
export M2_HOME=/apache-maven/
export PATH=\$PATH:\$M2_HOME/bin
EOF

set -u
source /etc/bashrc
