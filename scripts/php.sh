set -e -u

PHP_TIMEZONE="$1"
PHP_VERSION="$2"

PHP_PREFIX=
if [ $PHP_VERSION = "5.1" ]; then
    PHP_PREFIX="php"
elif [ $PHP_VERSION = "5.3" ]; then
    PHP_PREFIX="php53"
else
    PHP_PREFIX="php"$( echo $PHP_VERSION | sed 's/\.//g' )"w"

    if [ $( ls -1 /etc/yum.repos.d | grep webtatic | wc -l) = "0" ]; then
        rpm -Uvh http://mirror.webtatic.com/yum/el5/latest.rpm
    fi
fi

yum -y install $PHP_PREFIX \
               ${PHP_PREFIX}-mysql \
               ${PHP_PREFIX}-gd \
               ${PHP_PREFIX}-ldap \
               ${PHP_PREFIX}-devel \
               ${PHP_PREFIX}-xml \
               ${PHP_PREFIX}-mbstring

# Configure PHP
sed -i -r 's/memory_limit = 128M/memory_limit = 512M/' /etc/php.ini

# Install xdebug
TMP_DIR=`mktemp -d`
git clone -b XDEBUG_2_2_2 git@github.com:derickr/xdebug.git $TMP_DIR/xdebug

# Configure xdebug
cd $TMP_DIR/xdebug
phpize
./configure --enable-xdebug
make
make install

# Install xdebug
cp modules/xdebug.so /usr/lib/php/modules/
tee /etc/php.d/xdebug.ini << EOF
zend_extension=/usr/lib/php/modules/xdebug.so

;xdebug.profiler_enable=1
xdebug.profiler_output_dir=/vagrant/xdebug-profiling/
EOF

if [ ! -e /vagrant/xdebug-profiling/ ]; then
    mkdir /vagrant/xdebug-profiling/
fi

# Install APC
HAS_APC=0
if [ $PHP_VERSION = "5.1" ]; then
    HAS_APC=1
    yum -y install ${PHP_PREFIX}-pecl-apc
elif [ $PHP_VERSION = "5.3" ]; then
    HAS_APC=1
    yum -y install php-pear pcre-devel
    echo -e "no\nno\nno\nno\nno\nyes\n" | pecl install apc
fi

if [ $HAS_APC = "1" ]; then
    tee /etc/php.d/apc.ini << EOF
extension="apc.so"

;;; APC Configuration - START
apc.stat=1
apc.num_files_hint=0
apc.num_entries_hint=0
apc.ttl=5400
apc.user_ttl=5400
apc.shm_segments=1
;; Set this to the same as `sysctl -a | grep -E "shmall"`
apc.shm_size=268435456
;;; APC Configuration - END
EOF
fi

# Install PHP timezone
pecl install timezonedb || pecl upgrade timezonedb
tee /etc/php.d/timezonedb.ini << EOF
extension=timezonedb.so
EOF

# Make sure PHP can write to the sessions dir as vagrant
chown vagrant:vagrant -R /var/lib/php/session/

sed -ir 's/;date.timezone =/date.timezone = ${PHP_TIMEZONE/\//\\/}/' /etc/php.ini

# PHP Error Reporting Config
sed -ir "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php.ini
sed -ir "s/display_errors = .*/display_errors = On/" /etc/php.ini

# PHP Date Timezone
sed -ir "s/;date.timezone =.*/date.timezone = ${PHP_TIMEZONE/\//\\/}/" /etc/php.ini
sed -ie "s/;date.timezone =.*/date.timezone = ${PHP_TIMEZONE/\//\\/}/" /etc/php.ini

# Install Composer
php -r "readfile('https://getcomposer.org/installer');" | php
mv composer.phar /usr/bin/composer

tee -a /etc/bashrc << EOF

# Composer tools
export PATH=\$PATH:\$HOME/.composer/vendor/bin
EOF
