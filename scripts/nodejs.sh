set -e -u

# Install node.js and npm
mkdir /usr/local/node/
curl --insecure --silent http://nodejs.org/dist/v0.10.30/node-v0.10.30-linux-x86.tar.gz | tar --strip-components=1 --directory=/usr/local/node/ -zx

tee -a /etc/bashrc << EOF

# Node/NPM
export PATH=\$PATH:/usr/local/node/bin/
EOF

set +u
source /etc/bashrc
set -u

npm install -g bower
