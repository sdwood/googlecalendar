set -e -u

cd `mktemp -d`

JAVA_URL=http://download.oracle.com/otn-pub/java/jdk/6u45-b06/jdk-6u45-linux-i586-rpm.bin

# Install Java
wget --quiet  \
     --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" \
     --no-check-certificate \
     $JAVA_URL \
     -O jdk-linux-i586-rpm.bin

chmod 755 jdk-linux-i586-rpm.bin
./jdk-linux-i586-rpm.bin

rm -f *.rpm
rm -f ./jdk-linux-i586-rpm.bin

# Add the path to bashrc
tee -a /etc/bashrc << EOF

# Java
export JAVA_HOME=/usr/java/latest
export PATH=\$PATH:\$JAVA_HOME/bin/
EOF

set +u
source /etc/bashrc
