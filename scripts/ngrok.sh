set -e -u

# Download ngrok (http://ngrok.com) and make it available
wget "https://api.equinox.io/1/Applications/ap_pJSFC5wQYkAyI0FIVwKYs9h1hW/Updates/Asset/ngrok.zip?os=linux&arch=386&channel=stable" -O ngrok.zip
unzip ngrok.zip
rm -f ngrok.zip
mv ngrok /usr/local/bin
chmod 755 /usr/local/bin/ngrok
