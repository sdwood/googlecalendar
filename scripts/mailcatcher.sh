set -e -u

# Install RVM to get the latest Ruby
curl --insecure -sSL https://get.rvm.io | bash -s stable --ruby || true
source /etc/profile.d/rvm.sh

# Install Ruby 2.1.3
rvm install 2.1.3

# Install the gem
yum install -y sqlite-devel
gem install mailcatcher

# Make it start on boot
tee /etc/init.d/mailcatcher << EOF
#!/bin/bash
#
# Init file for Mailcatcher
#
# chkconfig: 2345 99 99
# description: Mailcatcher

SERVICE_NAME=mailcatcher

start() {
    echo -n "Starting \$SERVICE_NAME: "
    $( which mailcatcher ) --ip 0.0.0.0 --smtp-port 25
    return \$?
}

stop() {
    echo -n "Shutting down \$SERVICE_NAME: "
    ps aux | grep -v grep | grep ruby | grep mailcatcher | awk '{print "kill -9 "\$2}' | sh
    echo "OK"
    return 0
}

case "\$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    reload|restart)
        stop
        start
        ;;
    *)
        echo "Usage: \$SERVICE_NAME {start|stop|reload|restart}"
        exit 1
        ;;
esac
exit \$?
EOF

chmod 755 /etc/init.d/mailcatcher
