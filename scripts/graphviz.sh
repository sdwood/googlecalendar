set -e -u

# Install GraphViz
wget http://www.graphviz.org/graphviz-rhel.repo -O /etc/yum.repos.d/graphviz-rhel.repo
yum -y update
yum -y install graphviz ghostscript
