set -e -u

TOMCAT_VERSION="$1"

# Install Tomcat 6
mkdir /apache-tomcat
curl --insecure --silent https://archive.apache.org/dist/tomcat/tomcat-6/v6.0.9/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz | tar zx --strip-components=1 -C /apache-tomcat

TOMCAT_HOME=/apache-tomcat

sed -i -r 's/<Connector port="8080"/<Connector port="8080" URIEncoding="UTF-8"/' $TOMCAT_HOME/conf/server.xml
sed -i -r 's/<Connector port="8443"/<Connector port="8443" URIEncoding="UTF-8"/' $TOMCAT_HOME/conf/server.xml

# Setup the environment variables
tee -a /etc/bashrc << EOF

# Tomcat
export TOMCAT_HOME=$TOMCAT_HOME
EOF

# Create the init.d script

tee /etc/init.d/tomcat6 << EOF
#!/bin/bash
#
# Init file for tomcat
#
# chkconfig: 2345 99 99
# description: tomcat

# Source function library.
. /etc/init.d/functions
. /etc/bashrc

SERVICE_NAME=tomcat

start() {
	echo -n "Starting \$SERVICE_NAME: "
	\$TOMCAT_HOME/bin/startup.sh
    RETVAL=\$?
    [ "\$RETVAL" = 0 ] && touch /var/lock/subsys/\$SERVICE_NAME
    return \$RETVAL
}

stop() {
	echo -n "Shutting down \$SERVICE_NAME: "

	\$TOMCAT_HOME/bin/shutdown.sh
	ps aux | grep java | grep -v grep | awk '{print "kill -9 "\$2}' | sh

	[ -e /var/lock/subsys/\$SERVICE_NAME ] && rm -f /var/lock/subsys/\$SERVICE_NAME && echo "stopped" || echo "not started"
	return 0
}

case "\$1" in
    start)
		start
		;;
    stop)
		stop
		;;
    reload|restart)
    	stop
		start
		;;
    *)
		echo "Usage: \$SERVICE_NAME {start|stop|reload|restart}"
		exit 1
		;;
esac
exit $?
EOF
