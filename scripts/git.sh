set -e -u

GIT_VERSION=$1

# Add GitHub.com to the known hosts
tee -a /home/vagrant/.ssh/known_hosts << EOF
github.com,192.30.252.128 ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==
EOF
chown vagrant:vagrant -R /home/vagrant/.ssh/
cp /home/vagrant/.ssh/known_hosts /root/.ssh/known_hosts

# Compile git
cd `mktemp -d`
curl --insecure --silent https://www.kernel.org/pub/software/scm/git/git-${GIT_VERSION}.tar.gz | tar zx
cd git*
./configure && \
make && \
make install

# Add git autocompletion
if [ ! -e /etc/bash_completion.d/ ]; then
	mkdir /etc/bash_completion.d/
fi
curl --insecure --location --silent https://github.com/git/git/raw/master/contrib/completion/git-completion.bash | sudo tee /etc/bash_completion.d/git.sh
tee -a /etc/bashrc << EOF

# Git Autocompletion
source /etc/bash_completion.d/git.sh
EOF

## Git Config and set Owner
tee /home/vagrant/.gitconfig << EOF
[push]
        default = matching
[color]
        branch = auto
        diff = auto
        interactive = auto
        status = auto
        ui = auto
[advice]
        statusHints = false
[branch]
        autosetupmerge = true
[alias]
    #Basic
    st = status -sb
    co = checkout

    #Flow
    fs = flow feature start
    ff = flow feature finish

    #Infoz
    count = shortlog -sn
    df = diff --color
    wdf = diff --word-diff --color
    fdf = diff --name-only --color
    br = branch -a
    sh = show --pretty=format: --name-only

    #Log
    last = log -1 HEAD
    lg = log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --

    #Push/pull/commit
    pdev = push origin develop
    udev = pull origin develop
    amend = commit --amend
    cm = commit
    cmm = commit -m
EOF
chown vagrant:vagrant /home/vagrant/.gitconfig
cp /home/vagrant/.gitconfig /root/

# Common fixes for git
git config --global http.postBuffer 65536000

# Cache http credentials for one day while pull/push
git config --global credential.helper 'cache --timeout=86400'
