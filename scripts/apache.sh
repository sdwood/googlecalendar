set -e -u

# Install mod_ssl for Apache
yum -y install openssl mod_ssl

# Configure Apache
if [ -e /vagrant/apache-vhost ]; then
	ln -sf /vagrant/apache-vhost /etc/httpd/conf.d/vhost.conf
fi

# Sure Apache starts on boot
/sbin/chkconfig httpd on

# Make apache run as vagrant
sed -ir 's/User apache/User vagrant/' /etc/httpd/conf/httpd.conf
sed -ir 's/Group apache/Group vagrant/' /etc/httpd/conf/httpd.conf

# Allow all users to tail the logs
chmod 777 /var/log/httpd/

# Create a fake SSL cert
/etc/pki/tls/certs/make-dummy-cert /etc/pki/tls/certs/localhost

/etc/init.d/httpd restart
