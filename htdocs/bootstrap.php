<?php

// Include the autoloader
define('APP_PATH', __DIR__);
define('VENDOR_PATH', APP_PATH . '/vendor');
define('PUBLIC_PATH', APP_PATH . '/public');
define('LIBRARY_PATH', APP_PATH . '/library');

/*php configuration*/
ini_set('session.auto_start', 0);
date_default_timezone_set('America/Edmonton');

// Include the autoloader
require_once VENDOR_PATH . '/autoload.php';


// load config
\MRU\Config::load(APP_PATH . '/config/config.yml');
$config = \MRU\Config::getConfig();

   $googleCalendar = new MRU\Library\GoogleCalendar(
            $config['google']['clientid'], 
            $config['google']['clientsecret'],
            $config['google']['calendarid'],
            $config['google']['developerkey']);


//set up debuging
if ($config['app']['debug']) {
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
} else {
    error_reporting(0);
    ini_set('display_errors', 0);
}


$log = new \Monolog\Logger($config['log']['log_name']);
$log->pushHandler(new \Monolog\Handler\FirePHPHandler());

//start Silex
$app = new Silex\Application();
$app->register(new Whoops\Provider\Silex\WhoopsServiceProvider);
$app['googleCalendar'] = $googleCalendar;
$app['debug'] = $config['app']['debug'];
$app['config'] = $config;
$app['log'] = $log;
 