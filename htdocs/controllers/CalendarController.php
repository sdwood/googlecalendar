<?php
namespace MRU\Controller;

use MRU\Library\GoogleCalendar;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CalendarController
{
    protected $GoogleCalendar;


    /**
     * list all events
     */
    public function listevents(Request $request, Application $app) {
       $events = $app['googleCalendar']->getEvents();
        return $app['twig']->render('listevents.twig', array('data' => $events));
    }
    
    /**
     * check that user has oath token set
     */
    public function hasAuthToken(Request $request, Application $app) {
        if (!$request->getSession()->has('access_token')) {
            return $app->redirect('/oauth2callback');
        }
    }
    
    /**
     * oauth2callback handles google oauth response and set session
     */
    public function oauth2callback(Request $request, Application $app) {
        if (!$request->get('code')) {
            $auth_url = $app['googleCalendar']->getAouthUrl();
            return $app->redirect($auth_url);
        } 
        else {
            
            $app['session']->set('access_token', $app['googleCalendar']->getAccessToken());
            return $app->redirect('/listevents/');
        }
    }
}
