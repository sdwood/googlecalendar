<?php

/* layout.twig */
class __TwigTemplate_add5bc7368967a7c990bab0ac485966af9835485596a97b7946badebf6f08390 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <title>";
        // line 5
        echo twig_escape_filter($this->env, $this->getContext($context, "title"), "html", null, true);
        echo "</title>
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

        <link rel=\"shortcut icon\" href=\"";
        // line 8
        echo "/img/favicon/favicon.ico";
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"57x57\" href=\"";
        // line 9
        echo "/img/favicon/apple-touch-icon-57x57.png";
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"";
        // line 10
        echo "/img/favicon/apple-touch-icon-114x114.png";
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"";
        // line 11
        echo "/img/favicon/apple-touch-icon-72x72.png";
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"144x144\" href=\"";
        // line 12
        echo "/img/favicon/apple-touch-icon-144x144.png";
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"60x60\" href=\"";
        // line 13
        echo "/img/favicon/apple-touch-icon-60x60.png";
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"";
        // line 14
        echo "/img/favicon/apple-touch-icon-120x120.png";
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"";
        // line 15
        echo "/img/favicon/apple-touch-icon-76x76.png";
        echo "\">
        <link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"";
        // line 16
        echo "/img/favicon/apple-touch-icon-152x152.png";
        echo "\">
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 17
        echo "/img/favicon/favicon-196x196.png";
        echo "\" sizes=\"196x196\">
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 18
        echo "/img/favicon/favicon-160x160.png";
        echo "\" sizes=\"160x160\">
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 19
        echo "/img/favicon/favicon-96x96.png";
        echo "\" sizes=\"96x96\">
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 20
        echo "/img/favicon/favicon-16x16.png";
        echo "\" sizes=\"16x16\">
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 21
        echo "/img/favicon/favicon-32x32.png";
        echo "\" sizes=\"32x32\">
        <meta name=\"msapplication-TileColor\" content=\"#ffffff\">
        <meta name=\"msapplication-TileImage\" content=\"";
        // line 23
        echo "/img/favicon/mstile-144x144.png";
        echo "\">
        <meta name=\"msapplication-config\" content=\"";
        // line 24
        echo "/img/favicon/browserconfig.xml";
        echo "\">

        <link href=\"//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css\" rel=\"stylesheet\">
        <link href=\"//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css\" rel=\"stylesheet\">

        <link rel=\"stylesheet\" href=\"";
        // line 29
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('cache_buster')->getCallable(), array("/css/styles.css")), "html", null, true);
        echo "\" media=\"all\" />
    </head>
    <body>
        
    <nav class=\"navbar navbar-fixed-top\">
      <div class=\"container-fluid\">
        <div class=\"navbar-header\">
          <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
            <span class=\"sr-only\">Toggle navigation</span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
          </button>
       
          <a class=\"navbar-brand\" href=\"#\">
                    <img src=\"/img/header-logo.png\" alt=\"\">
                </a>
                <a class=\"navbar-brand\" href=\"#\"> ";
        // line 46
        echo twig_escape_filter($this->env, $this->getContext($context, "title"), "html", null, true);
        echo "</a>
        </div>
        <div id=\"navbar\" class=\"navbar-collapse collapse\">
          <ul class=\"nav navbar-nav navbar-right\">
            <li><a href=\"#\">Sign out</a></li>
            <li><a href=\"#\">Help</a></li>
          </ul>
          <form class=\"navbar-form navbar-right\">
            <input type=\"text\" class=\"form-control\" placeholder=\"Search...\">
          </form>
        </div>
      </div>
    </nav>

    <div class=\"container-fluid\">
      <div class=\"row\">
        <div class=\"col-sm-3 col-md-2 sidebar\">
          <ul class=\"nav nav-sidebar\">
            <li class=\"active\"><a href=\"#\">List Events </a></li>
            <li><a href=\"#\">test</a></li>
          </ul>

        </div>
        <div class=\"col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main\">
               <div class=\"content\">
                        ";
        // line 71
        $this->displayBlock('content', $context, $blocks);
        // line 72
        echo "                    </div>
    </div>
        <script src=\"//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js\"></script>
        <script src=\"//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 76
        echo "/js/javascript.js";
        echo "\"></script>

        ";
        // line 78
        $this->displayBlock('javascript', $context, $blocks);
        // line 79
        echo "
        ";
        // line 80
        if ((!$this->getAttribute($this->getContext($context, "app"), "debug"))) {
            // line 81
            echo "            <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
                ga('create', '";
            // line 86
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "site"), "google_analytics_id"), "html", null, true);
            echo "', '";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getContext($context, "app"), "server_name"), "html", null, true);
            echo "');
                ga('send', 'pageview');
            </script>
        ";
        }
        // line 90
        echo "    </body>
</html>
";
    }

    // line 71
    public function block_content($context, array $blocks = array())
    {
    }

    // line 78
    public function block_javascript($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "layout.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 78,  192 => 71,  186 => 90,  177 => 86,  170 => 81,  168 => 80,  165 => 79,  163 => 78,  158 => 76,  152 => 72,  150 => 71,  122 => 46,  102 => 29,  94 => 24,  90 => 23,  85 => 21,  81 => 20,  77 => 19,  73 => 18,  69 => 17,  65 => 16,  61 => 15,  57 => 14,  53 => 13,  49 => 12,  45 => 11,  41 => 10,  37 => 9,  33 => 8,  27 => 5,  21 => 1,);
    }
}
