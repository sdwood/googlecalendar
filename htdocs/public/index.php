<?php
ini_set('display_errors', 1);

require_once __DIR__ . '/../bootstrap.php';

// Add providers to silex
$app->register(new \Silex\Provider\UrlGeneratorServiceProvider());
$app->register(new \Silex\Provider\FormServiceProvider());
$app->register(new \Silex\Provider\ValidatorServiceProvider());
$app->register(new \Silex\Provider\TranslationServiceProvider() , array(
    'translator.domains' => array() ,
));

// Setup the session provider
$app->register(new Silex\Provider\SessionServiceProvider());
$app['session.storage.options'] = array(
    'name' => $app['config']['auth']['session_name'],
    'cookie_lifetime' => $app['config']['auth']['cookie_lifetime']
);
$app['session.storage.save_path'] = dirname(__DIR__) . '/tmp/sessions';

// Setup template engine
$app->register(new \Silex\Provider\TwigServiceProvider() , array(
    'twig.path' => realpath(APP_PATH . '/views/') ,
    'twig.form.templates' => array(
        'form_div_layout.html.twig',
        
        // part of symfony/twig-bridge
        'forms.twig'
        
        // custom templates
        
        
    ) ,
    'twig.options' => array(
        'debug' => $app['debug'],
        'cache' => APP_PATH . '/tmp/twig-cache/',
        'strict_variables' => true
    )
));
$app['twig'] = $app->share($app->extend('twig', function ($twig, $app) {
    
    // Cache Bust CSS and JS URLs
    $twig->addFilter(new \Twig_SimpleFilter('cache_buster', function ($url) {
        $filePath = PUBLIC_PATH . $url;
        if (file_exists($filePath)) {
            $lastModified = filemtime($filePath);
            if (preg_match('/^(.+?)\.([a-z]+)$/', $url, $matches)) {
                $url = $matches[1] . '-' . $lastModified . '.' . $matches[2];
            }
        }
        return $url;
    }));
    return $twig;
}));

$app['twig']->addGlobal('title', $config['app']['title']);

/**
 * Routes
 */
$app->match('/', '\\MRU\\Controller\\CalendarController::listevents')->method('GET|POST')->before('\\MRU\\Controller\\CalendarController::hasAuthToken');
$app->match('/listevents/', '\\MRU\\Controller\\CalendarController::listevents')->method('GET|POST')->before('\\MRU\\Controller\\CalendarController::hasAuthToken');
$app->match('/oauth2callback/', '\\MRU\\Controller\\CalendarController::oauth2callback')->method('GET|POST');

$app->run();
