<?php
namespace MRU\Library;
class GoogleCalendar
{
    protected $clientId;
    protected $clientsecret;
    protected $calendarid;
    protected $googleCalendarService;
    protected $googleClient;
    
    function __construct($clientId, $clientSecret,$calendarid,$developerkey) {
        $this->clientId = $clientId;
        $this->clientSecret = $clientSecret;
        $this->calendarid = $calendarid;
        
        $this->googleClient = new \Google_Client();
        $this->googleClient->setAccessType('online');
        $this->googleClient->setClientId($this->clientId);
        $this->googleClient->setClientSecret($this->clientSecret);
        $this->googleClient->setDeveloperKey($developerkey);
        $this->googleClient->addScope('https://www.googleapis.com/auth/calendar');

        $this->googleClient->setRedirectUri('http://localhost/oauth2callback/');
        $this->googleCalendarService = new \Google_Service_Calendar($this->googleClient);
    }  

    
    function setApplicationName($applicationName) {
        $this->googleClient->setApplicationName($applicationName);
    }
    
    function setRedirectUri($redirectUri) {
        $this->googleClient->setRedirectUri($redirectUri);
    }
    
   
    function getAouthUrl() {
        return $this->googleClient->createAuthUrl();
    }
    
    function authenticate($code) {
        return $this->googleCalendarService->authenticate($code);
    }
    
    function getAccessToken() {
        return $this->googleClient->getAccessToken();
    }
    
    function getEvents($options=array()) {
        return $this->googleCalendarService->events->listEvents($this->calendarid, $options);
    }
}
