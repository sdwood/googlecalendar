# -*- mode: ruby -*-
# vi: set ft=ruby :

###
# Configuration
###

# URL to the VM image
vm_box_url = "https://dl.dropboxusercontent.com/s/hwk6fm1cn255w17/centos_56_32bit.box"

# Github Settings
git_version = "2.2.0"
github_oauth_token = "a153ebbeb52d45bc411decce8715488f65d3964f"
github_url = "https://code.mtroyal.ca/api/v3/repos/webteam/vagrant/tarball/master?access_token=#{github_oauth_token}"
webteam_vagrant_path = "/root/vagrant"

# Server Configuration
hostname = "calendar"

# Set a local private network IP address. Use something in the 192.168.5.* range, as some scripts depend on that range
server_ip = "192.168.5.101"
server_memory = "256" # MB
server_swap_size = "512" # Options: false | int (MB) - Guideline: Between one or two times the server_memory

# Server Timezone
server_timezone = "America/Edmonton"

# Web Server Configuration
tomcat_version = "6.0.9"

# MySQL Configuration
mysql_root_password = "password"   # We'll assume user "root"

# Set the following values to have a MySQL database created for you
mysql_user_database = "event"
mysql_user_name = "admin"
mysql_user_password = "events"

# Mailtrap configuration
mailtrap_user = "200382afbf4e515f4"
mailtrap_password = "90011dc20c47f5"

# Languages and Packages
php_version = "5.3"

Vagrant.configure("2") do |config|
  config.vm.box = "mru"
  config.vm.box_url = vm_box_url

  # Create a hostname
  config.vm.hostname = "calendar"

  # Create a static IP
  config.vm.network :private_network, ip: server_ip

  # Use NFS for the shared folder
  config.vm.synced_folder ".", "/vagrant", id: "core", :nfs => true, :mount_options => ['dmode=0775', 'fmode=0664']


  # Make sure the VirtualBox Guest Additions are up to date
  # This requires the vbguest plugin to be installed: https://github.com/dotless-de/vagrant-vbguest
  config.vbguest.auto_update = true

  # VirtualBox settings
  config.vm.provider :virtualbox do |vb|
    vb.name = hostname

    # Set server memory
    vb.customize ["modifyvm", :id, "--memory", server_memory]

    # Set the timesync threshold to 10 seconds, instead of the default 20 minutes.
    # If the clock gets more than 15 minutes out of sync (due to your laptop going
    # to sleep for instance), then some 3rd party services will reject requests.
    vb.customize ["guestproperty", "set", :id, "/VirtualBox/GuestAdd/VBoxService/--timesync-set-threshold", 10000]

    # Enable symlinks in the /vagrant/ directory
    vb.customize ['setextradata', :id, 'VBoxInternal2/SharedFoldersEnableSymlinksCreate/v-root', '1' ]
  end

  # Copy over your SSH keys to the VM
  config.vm.provision "file", source: "~/.ssh/id_rsa", destination: "/home/vagrant/.ssh/id_rsa"
  config.vm.provision "file", source: "~/.ssh/id_rsa.pub", destination: "/home/vagrant/.ssh/id_rsa.pub"

  config.vm.provision "shell",  inline: <<SCRIPT
      # Restart the network so it doesn't hang
      /etc/init.d/network restart

      # Download the scripts to a local directory
      [ -e #{webteam_vagrant_path} ] && rm -rf #{webteam_vagrant_path} || true
      mkdir #{webteam_vagrant_path}
      curl --insecure --location --silent #{github_url}  | tar zx --strip-components=1 -C #{webteam_vagrant_path}
      SCRIPTS_PATH=#{webteam_vagrant_path}/scripts


      ############
      # Base Items
      ############

      # Provision Base Packages
      $SCRIPTS_PATH/base.sh #{hostname} #{server_swap_size} #{server_timezone}

      # Provision Git
      $SCRIPTS_PATH/git.sh #{git_version}

      # Provision Mailcatcher (http://mailcatcher.me/)
      # $SCRIPTS_PATH/mailcatcher.sh

      # Provision Mailtrap (http://mailtrap.io/)
      $SCRIPTS_PATH/mailtrap.sh #{mailtrap_user} #{mailtrap_password}


      #############
      # Web Servers
      #############

      # Provision Apache
      $SCRIPTS_PATH/apache.sh

      # Provision Apache Tomcat
      # $SCRIPTS_PATH/tomcat.sh #{tomcat_version}


      ###########
      # Databases
      ###########

      # Provision MySQL
      $SCRIPTS_PATH/mysql.sh #{mysql_root_password} #{mysql_user_database} #{mysql_user_name} #{mysql_user_password}

      # Provision Postgres
      # $SCRIPTS_PATH/postgres.sh


      #######################
      # Programming Languages
      #######################

      # Provision PHP
      $SCRIPTS_PATH/php.sh #{server_timezone} #{php_version}

      # Provision Node.js
      $SCRIPTS_PATH/nodejs.sh


      ###########
      # Utilities
      ###########

      # Provision Apache Ant
      # $SCRIPTS_PATH/ant.sh

      # Provision Apache Maven
      # $SCRIPTS_PATH/maven.sh

      # Provision Graphviz (http://graphviz.org/)
      # $SCRIPTS_PATH/graphviz.sh

      # Provision Java JRE
      # $SCRIPTS_PATH/java.sh

      # Provision NGrok (https://ngrok.com/)
      $SCRIPTS_PATH/ngrok.sh

      # Cleanup
      rm -rf #{webteam_vagrant_path}
SCRIPT


  ####
  # Local Scripts
  # Any local scripts you may want to run post-provisioning.
  # Add these to the same directory as the Vagrantfile.
  ##########
  # config.vm.provision "shell", path: "./local-script.sh"

end
